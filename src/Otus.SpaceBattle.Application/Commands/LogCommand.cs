﻿namespace Otus.SpaceBattle.Application.Commands
{
    public class LogCommand : ICommand
    {
        ICommand _outerCmd;
        Exception _outerEx;
        List<string> _logs;
        public LogCommand(ICommand cmd, Exception ex, List<string> log)
        {
            this._outerCmd = cmd;
            this._outerEx = ex;
            this._logs = log;
        }
        public void Execute()
        {
            _logs.Add($"{_outerCmd.ToString()} fails with message {_outerEx.Message}"); 
        }
    }
}
