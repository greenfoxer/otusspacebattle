﻿namespace Otus.SpaceBattle.Application.Commands
{
    public class Repeat2TimesCommand : ICommand
    {
        private ICommand cmd;
        private Exception ex;

        public Repeat2TimesCommand(ICommand cmd, Exception ex)
        {
            this.cmd = cmd;
            this.ex = ex;
        }

        public void Execute()
        {
            cmd.Execute();
        }
        public override string ToString()
        {
            return cmd.ToString();
        }
    }
}
