﻿using Otus.SpaceBattle.Core.Abstractions;

namespace Otus.SpaceBattle.Application.Commands
{
    public class SetupCommand : ICommand
    {
        IUObject _object;
        object _value;
        string _prop;
        public SetupCommand(IUObject obj, string prop, object value)
        {
            _object = obj;
            _value = value;
            _prop = prop;
        }
        public void Execute()
        {
            _object.SetPropertyOrThrows(_prop, _value);
        }
    }
}
