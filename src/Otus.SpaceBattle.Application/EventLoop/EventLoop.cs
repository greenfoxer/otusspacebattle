﻿using Otus.SpaceBattle.IoC;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Otus.SpaceBattle.Application.EventLoop
{
    public class EventLoop : IEventLoop
    {
        public string Name { get; private set; }
        public Func<bool> CanContinue { get; set; } = () => true;

        public EventLoop(string name)
        {
            Name = name;    
        }
        public void Start()
        {
            var globalQueue = IoCContainer.Resolve<BlockingCollection<ICommand>>(IoCStringConstants.GlobalCommandsQueue);
            var internalCommandQueue = new Queue<ICommand>();
            while(CanContinue())
            {
                if(internalCommandQueue.Count == 0)
                {
                    var cmd = globalQueue.Take();
                    internalCommandQueue.Enqueue(cmd);
                }
                else
                {
                    if(globalQueue.TryTake(out var cmd)){
                        internalCommandQueue.Enqueue(cmd);
                    }
                }
                internalCommandQueue.Dequeue().Execute();
            }
        }

    }
}
