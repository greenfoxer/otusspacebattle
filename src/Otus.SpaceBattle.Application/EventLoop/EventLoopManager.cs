﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Otus.SpaceBattle.Application.EventLoop
{
    public static class EventLoopManager
    {
        private static List<Thread> _eventLoops = new List<Thread>();
        private static BlockingCollection<IEventLoop> _eventsLoopList = new BlockingCollection<IEventLoop>();
        private static ThreadLocal<IEventLoop> _currentEventLoop = new ThreadLocal<IEventLoop>();

        public static void StartEventLoop(string name)
        {
            var loop = new EventLoop(name);
            _eventsLoopList.Add(loop);
            Thread newEventLoop = new Thread(() => { _currentEventLoop.Value = loop; loop.Start(); });
            newEventLoop.Start();
        }
        internal static IEventLoop CurrentThreadEventLoop => _currentEventLoop.Value;
    }
}
