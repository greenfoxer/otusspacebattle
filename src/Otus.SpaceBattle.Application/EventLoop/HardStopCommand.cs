﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Otus.SpaceBattle.Application.EventLoop
{
    public class HardStopCommand : ICommand
    {
        public void Execute()
        {
            EventLoopManager.CurrentThreadEventLoop.CanContinue = () => false;
        }
    }
}
