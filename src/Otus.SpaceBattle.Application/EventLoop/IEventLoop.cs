﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Otus.SpaceBattle.Application.EventLoop
{
    public interface IEventLoop
    {
        string Name { get; }
        Func<bool> CanContinue { get; set; }
        void Start();
    }
}
