﻿using Otus.SpaceBattle.IoC;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Otus.SpaceBattle.Application.EventLoop
{
    public class SoftStopCommand : ICommand
    {
        public SoftStopCommand()
        {

        }
        public void Execute()
        {
            var queue = IoCContainer.Resolve<BlockingCollection<ICommand>>(IoCStringConstants.GlobalCommandsQueue);
            IEventLoop el = EventLoopManager.CurrentThreadEventLoop;
            el.CanContinue = () =>  queue.Count > 0;
        }
    }
}
