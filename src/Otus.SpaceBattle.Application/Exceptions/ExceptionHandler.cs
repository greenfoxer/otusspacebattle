﻿using Otus.SpaceBattle.IoC.Abstractions;

namespace Otus.SpaceBattle.Application.Exceptions
{
    public class ExceptionHandler : IExceptionHandler
    {
        public ExceptionHandler()
        {
            _ruleSet = new Dictionary<Type, IDictionary<Type, Action<ICommand, Exception>>>();
        }
        private IDictionary<Type, IDictionary<Type, Action<ICommand, Exception>>> _ruleSet;
        public void Handle(ICommand cmd, Exception ex)
        {
            if(_ruleSet.TryGetValue(cmd.GetType(), out var value))
            {
                if(value.TryGetValue(ex.GetType(), out var action))
                    action(cmd, ex);
                else
                    value[typeof(Exception)](cmd, ex);
            }
        }

        public void Setup(Type cmd, Type ex, Action<ICommand, Exception> onFail)
        {
            if (!_ruleSet.ContainsKey(cmd))
            {
                _ruleSet.Add(cmd, new Dictionary<Type, Action<ICommand, Exception>>());
                _ruleSet[cmd].Add(typeof(Exception), (cmd, ex) => { Console.WriteLine("Unhandled exception"); });
            }

            if (!_ruleSet[cmd].ContainsKey(ex))
                _ruleSet[cmd].Add(ex, onFail);
        }
    }
}
