﻿namespace Otus.SpaceBattle.Application.Exceptions
{
    public interface IExceptionHandler
    {
        void Setup(Type cmd, Type ex, Action<ICommand, Exception> onFail);
        void Handle(ICommand cmd, Exception ex);
    }
}
