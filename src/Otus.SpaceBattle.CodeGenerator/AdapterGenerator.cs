﻿using Microsoft.CodeAnalysis.CSharp;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using Otus.SpaceBattle.CommandBase;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Otus.SpaceBattle.CodeGenerator
{
    public class AdapterGenerator
    {
        private readonly Type _type;
        string[] _usings;
        string _className;
        public AdapterGenerator(Type interfaceToGenerate, params string[] requredUsings)
        {
            _type = interfaceToGenerate;
            _usings = requredUsings;
            _className = $"{_type.Name}Adapter";
        }
        // конструктор и набор закрытых полей - вещи уникальные, остальное, в целом, можно получить из интерфейса.
        public string Generate()
        {
            var ia = new InterfaceAnalyzer(_type);

            var builder = new SyntaxTreeBuilder();
            var payload = builder
                .PrepareNamespace($"GeneratedAdapters")
                .PrepareUsings("System", "Otus.SpaceBattle.Core.Abstractions", "System.Numerics", "Otus.SpaceBattle.IoC", "Otus.SpaceBattle.CommandBase")
                .PrepareUsings(_usings)
                .PrepareClass(_className, _type.Name)
                .PreparePrivateFields(new Tuple<string, string>("IUObject", "_object"))
                .PrepareCustomConstructor(GetAdapterConstructor(_className))
                .PrepareProperties(GeneratePropertyMethodsBody, ia.InterfaceProps.ToArray())
                .PrepareMethods(GenerateMethodBody, ia.InterfaceMethods.ToArray())
                .GenerateCode();

            return payload;
        }

        public Type GetCompiledType()
        {
            return DynamicClassLoader.CompileCodeAndExtractClass(_className, this.Generate(), "GeneratedAdapters."+_className);
        }
        private string GenerateMethodBody(string typeToReturn, string name, params string[] parameters)
        {
            string bodyBase = "IoCContainer.Resolve<{0}>(\"" + _type.Name + ".{1}\",_object {2});";
            var genericParameter = typeToReturn == "void" ? "ICommand" : typeToReturn;
            var joinedParameters = parameters == null ? string.Empty : string.Join(',', parameters).Trim();
            joinedParameters = string.IsNullOrWhiteSpace(joinedParameters) ? string.Empty : "," + joinedParameters;
            var result = string.Format(bodyBase, genericParameter, name, joinedParameters);
            if (genericParameter == "ICommand")
                result = result.Replace(";", ".Execute();");
            else
                result = $"return {result}";

            return result;
        }
        private string GeneratePropertyMethodsBody(string typeToReturn, string name, string access, params string[] parameters)
        {
            string bodyBase = "IoCContainer.Resolve<{0}>(\"" + _type.Name + ".{1}.{2}\",_object {3});";
            var genericParameter = typeToReturn == "void" ? "ICommand" : typeToReturn;
            var joinedParameters = parameters == null ? string.Empty : string.Join(',', parameters).Trim();
            joinedParameters = string.IsNullOrWhiteSpace(joinedParameters) ? string.Empty : "," + joinedParameters;
            var result = string.Format(bodyBase, genericParameter, name, access, joinedParameters);
            if (access == "Set")
                result = result.Replace(";", ".Execute();");
            else
                result = $"return {result}";

            return result;
        }
        private static ConstructorDeclarationSyntax GetAdapterConstructor(string className)
        {
            return SyntaxFactory.ConstructorDeclaration(className)
                    .AddParameterListParameters(
                        SyntaxFactory.Parameter(SyntaxFactory.Identifier("uobject"))
                            .WithType(SyntaxFactory.ParseTypeName("IUObject")))
                    .WithBody(SyntaxFactory.Block(SyntaxFactory.ParseStatement("_object = uobject;")))
                    .AddModifiers(SyntaxFactory.Token(SyntaxKind.PublicKeyword));
        }
    }
}
