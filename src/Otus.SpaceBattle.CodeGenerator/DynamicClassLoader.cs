﻿using Microsoft.CodeAnalysis.CSharp;
using Microsoft.CodeAnalysis.Emit;
using Microsoft.CodeAnalysis;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Otus.SpaceBattle.CodeGenerator
{
    /// <summary>
    /// Used to load classes dynamically from C# code files.
    /// </summary>
    static class DynamicClassLoader
    {
        // loaded assemblies
        static Dictionary<string, Assembly> _loadedAssemblies = new Dictionary<string, Assembly>();

        /// <summary>
        /// Compile assembly, or return instance from cache.
        /// </summary>
        public static Assembly Compile(string codeIdentifier, string code)
        {
            // get from cache
            Assembly ret = null;
            if (_loadedAssemblies.TryGetValue(codeIdentifier, out ret))
            {
                return ret;
            }

            // create syntax tree from code
            SyntaxTree syntaxTree = CSharpSyntaxTree.ParseText(code);

            //The location of the .NET assemblies
            var assemblyPath = Path.GetDirectoryName(typeof(object).Assembly.Location);

            // get randomly generated assembly name
            string assemblyName = Path.GetRandomFileName();

            AddNetCoreDefaultReferences();

            foreach (var ass in System.Runtime.Loader.AssemblyLoadContext.Default.Assemblies.Where(t => t.FullName.StartsWith("Otus")))
                References.Add(MetadataReference.CreateFromFile(ass.Location));

            // setup compiler
            CSharpCompilation compilation = CSharpCompilation.Create(
                assemblyName,
                syntaxTrees: new[] { syntaxTree },
                references: References,
                options: new CSharpCompilationOptions(OutputKind.DynamicallyLinkedLibrary));
            
            // compile code
            using (var ms = new MemoryStream())
            {
                // first compile
                EmitResult result = compilation.Emit(ms);

                // if failed, get error message
                if (!result.Success)
                {
                    IEnumerable<Diagnostic> failures = result.Diagnostics.Where(diagnostic =>
                        diagnostic.IsWarningAsError ||
                        diagnostic.Severity == DiagnosticSeverity.Error);

                    foreach (Diagnostic diagnostic in failures)
                    {
                        throw new Exception(string.Format("Failed to compile code '{0}'! {1}: {2}", codeIdentifier, diagnostic.Id, diagnostic.GetMessage()));
                    }
                    throw new Exception("Unknown error while compiling code '" + codeIdentifier + "'!");
                }
                // on success, load into assembly
                else
                {
                    // load assembly and add to cache
                    ms.Seek(0, SeekOrigin.Begin);
                    ret = Assembly.Load(ms.ToArray());
                    _loadedAssemblies[codeIdentifier] = ret;

                    // return assembly
                    return ret;
                }
            }
        }

        /// <summary>
        /// Compile code and extract a class from it.
        /// </summary>
        /// <param name="path">Script file path.</param>
        /// <param name="className">Class name to get.</param>
        public static Type CompileCodeAndExtractClass(string path, string className)
        {
            return CompileCodeAndExtractClass(path, File.ReadAllText(path), className);
        }

        /// <summary>
        /// Compile code and extract a class from it.
        /// </summary>
        /// <param name="codeIdentifier">Unique identifier for the code.</param>
        /// <param name="code">Code to compile.</param>
        /// <param name="className">Class name to extract. Can be null to just load and compile the code.</param>
        public static Type CompileCodeAndExtractClass(string codeIdentifier, string code, string className)
        {
            // if debug mode and class is part of project, return as-is
#if DEBUG
            if (className == null) { return null; }
            var debugRet = Assembly.GetExecutingAssembly().GetType(className);
            if (debugRet != null)
            {
                return debugRet;
            }
#endif
            // return value
            Type ret = null;

            // extract class
            if (!string.IsNullOrEmpty(className))
            {
                // get assembly and try to extract class
                var assembly = Compile(codeIdentifier, code);
                ret = assembly.GetType(className);

                // didn't find main class?
                if (ret == null)
                {
                    throw new Exception("Fail to find class '" + className + "' in code '" + codeIdentifier + "'!");
                }
            }

            // return class
            return ret;
        }

        public static HashSet<PortableExecutableReference> References { get; set; } =
            new HashSet<PortableExecutableReference>();

        public static bool AddAssembly(Type type)
        {
            try
            {
                if (References.Any(r => r.FilePath == type.Assembly.Location))
                    return true;

                var systemReference = MetadataReference.CreateFromFile(type.Assembly.Location);
                References.Add(systemReference);
            }
            catch
            {
                return false;
            }

            return true;
        }
        public static bool AddAssembly(string assemblyDll)
        {
            if (string.IsNullOrEmpty(assemblyDll)) return false;

            var file = Path.GetFullPath(assemblyDll);

            if (!File.Exists(file))
            {
                // check framework or dedicated runtime app folder
                var path = Path.GetDirectoryName(typeof(object).Assembly.Location);
                file = Path.Combine(path, assemblyDll);
                if (!File.Exists(file))
                    return false;
            }

            if (References.Any(r => r.FilePath == file)) return true;

            try
            {
                var reference = MetadataReference.CreateFromFile(file);
                References.Add(reference);
            }
            catch
            {
                return false;
            }

            return true;
        }
        public static void AddAssemblies(params string[] assemblies)
        {
            foreach (var file in assemblies)
                AddAssembly(file);
        }
        public static void AddNetCoreDefaultReferences()
        {
            var rtPath = Path.GetDirectoryName(typeof(object).Assembly.Location) +
                         Path.DirectorySeparatorChar;

            AddAssemblies(
                rtPath + "System.Private.CoreLib.dll",
                rtPath + "System.Runtime.dll",
                rtPath + "System.Console.dll",
                rtPath + "netstandard.dll",

                rtPath + "System.Text.RegularExpressions.dll", // IMPORTANT!
                rtPath + "System.Linq.dll",
                rtPath + "System.Linq.Expressions.dll", // IMPORTANT!

                rtPath + "System.IO.dll",
                rtPath + "System.Net.Primitives.dll",
                rtPath + "System.Net.Http.dll",
                rtPath + "System.Private.Uri.dll",
                rtPath + "System.Reflection.dll",
                rtPath + "System.ComponentModel.Primitives.dll",
                rtPath + "System.Globalization.dll",
                rtPath + "System.Collections.Concurrent.dll",
                rtPath + "System.Collections.NonGeneric.dll",
                rtPath + "Microsoft.CSharp.dll",
                rtPath + "System.Numerics.Vectors.dll"
            );
        }
    }
}
