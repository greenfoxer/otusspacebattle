﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Otus.SpaceBattle.CodeGenerator
{
    public class InterfaceAnalyzer
    {
        public InterfaceAnalyzer(Type interfaceToAnalyze)
        {
            if (interfaceToAnalyze.IsInterface)
            {
                this.InterfaceType = interfaceToAnalyze;
                Analyze();
            }
            else
                throw new InvalidDataException($"{interfaceToAnalyze.Name} is not an interface!");
        }

        public Type InterfaceType { get; }
        public List<PropertyInfo> InterfaceProps;
        public List<MethodInfo> InterfaceMethods;

        public void Analyze()
        {
            // get interface properties
            InterfaceProps = new List<PropertyInfo>();

            var typeProperties = InterfaceType.GetProperties(
               BindingFlags.FlattenHierarchy
               | BindingFlags.Public
               | BindingFlags.Instance);
            
            var newPropertyInfos = typeProperties
                .Where(x => !InterfaceProps.Contains(x));

            InterfaceProps.InsertRange(0, newPropertyInfos);

            // get interface methods
            InterfaceMethods = new List<MethodInfo>();

            var typeMethods = InterfaceType.GetMethods(
               BindingFlags.FlattenHierarchy
               | BindingFlags.Public
               | BindingFlags.Instance);

            var newMethodInfos = typeMethods
                .Where(x => !InterfaceMethods.Contains(x) && !x.IsSpecialName);

            InterfaceMethods.InsertRange(0, newMethodInfos);
        }
    }
}
