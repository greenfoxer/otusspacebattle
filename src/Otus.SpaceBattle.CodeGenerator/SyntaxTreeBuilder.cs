﻿using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Reflection.Metadata;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace Otus.SpaceBattle.CodeGenerator
{
    public class SyntaxTreeBuilder
    {
        private NamespaceDeclarationSyntax _namespaceDeclarationSyntax;
        private List<PropertyDeclarationSyntax> _properties = new List<PropertyDeclarationSyntax>();
        private List<MethodDeclarationSyntax> _methods = new List<MethodDeclarationSyntax>();
        private ClassDeclarationSyntax _classDeclarationSyntax;
        private List<FieldDeclarationSyntax> _fieldDeclarationSyntaxList;
        private ConstructorDeclarationSyntax _constructorDeclarationSyntax;
        public SyntaxTreeBuilder PrepareNamespace(string createdNamespace)
        {
            // Create a namespace: (namespace {nameSpace})
            _namespaceDeclarationSyntax = SyntaxFactory.NamespaceDeclaration(SyntaxFactory.ParseName(createdNamespace)).NormalizeWhitespace();
               
            return this;
        }
        public SyntaxTreeBuilder PrepareUsings(params string[] requiredUsings)
        {
            // Add System using statement: (using System)
            _namespaceDeclarationSyntax = _namespaceDeclarationSyntax.AddUsings(requiredUsings.Select(t => SyntaxFactory.UsingDirective(SyntaxFactory.ParseName(t))).ToArray());

            return this;
        }
        public SyntaxTreeBuilder PrepareClass(string className, string interfaceName)
        {
            _classDeclarationSyntax = SyntaxFactory.ClassDeclaration(className);

            // Add the public modifier: (public class {className})
            _classDeclarationSyntax = _classDeclarationSyntax.AddModifiers(SyntaxFactory.Token(SyntaxKind.PublicKeyword));

            // Implement interface from data: (public class {className} : {data.InterfaceName})
            _classDeclarationSyntax = _classDeclarationSyntax.AddBaseListTypes(
                SyntaxFactory.SimpleBaseType(SyntaxFactory.ParseTypeName(interfaceName)));
            return this;
        }
        public SyntaxTreeBuilder PrepareDefaultConstructor(string className, string body)
        {
            _constructorDeclarationSyntax = SyntaxFactory.ConstructorDeclaration(className)
                .WithBody(SyntaxFactory.Block(SyntaxFactory.ParseStatement(body)))
                .AddModifiers(SyntaxFactory.Token(SyntaxKind.PublicKeyword));

            return this;
        }
        internal SyntaxTreeBuilder PrepareCustomConstructor(ConstructorDeclarationSyntax constructorDeclarationSyntax)
        {
            _constructorDeclarationSyntax = constructorDeclarationSyntax;

            return this;
        }
        public SyntaxTreeBuilder PreparePrivateFields(params Tuple<string, string>[] fields)
        {
            // Create a IUObject variable: (IUObject _object;)
            _fieldDeclarationSyntaxList = new List<FieldDeclarationSyntax>();
            foreach (var field in fields)
            {
                var variableDeclaration = SyntaxFactory.VariableDeclaration(SyntaxFactory.ParseTypeName(field.Item1))
                    .AddVariables(SyntaxFactory.VariableDeclarator(field.Item2));

                // Create a field declaration: (private IUObject _object;)
                _fieldDeclarationSyntaxList.Add(SyntaxFactory.FieldDeclaration(variableDeclaration)
                    .AddModifiers(SyntaxFactory.Token(SyntaxKind.PrivateKeyword)));
            }
            return this;
        }
        public SyntaxTreeBuilder PrepareProperties(Func<string, string, string, string[], string> generateMethodBody, params PropertyInfo[] properties)
        {
            foreach (var prop in properties)
            {
                var syntaxGet = SyntaxFactory.ParseStatement(generateMethodBody(prop.PropertyType.GetCSTypeName(), prop.Name, "Get", null));
                var syntaxSet = SyntaxFactory.ParseStatement(generateMethodBody("void", prop.Name, "Set",new string[] { "value" }));
                _properties.Add(
                    // Create a Property: (public int Quantity { get; set; })
                    SyntaxFactory.PropertyDeclaration(SyntaxFactory.ParseTypeName(prop.PropertyType.Name), prop.Name)
                        .AddModifiers(SyntaxFactory.Token(SyntaxKind.PublicKeyword))
                        .AddAccessorListAccessors(
                            SyntaxFactory.AccessorDeclaration(SyntaxKind.GetAccessorDeclaration, SyntaxFactory.Block(syntaxGet)).WithSemicolonToken(SyntaxFactory.Token(SyntaxKind.None)),
                            SyntaxFactory.AccessorDeclaration(SyntaxKind.SetAccessorDeclaration, SyntaxFactory.Block(syntaxSet)).WithSemicolonToken(SyntaxFactory.Token(SyntaxKind.None)))
                    );
            }
            return this;
        }
        public SyntaxTreeBuilder PrepareMethods(Func<string, string, string[], string> generateMethodBody, params MethodInfo[] methods)
        {
            // Create a statement with the body of a method.
            foreach (var method in methods)
            {
                var parameters = method.GetParameters().ToArray();
                var body = generateMethodBody(method.ReturnType.GetCSTypeName(), method.Name, parameters.Select(t => t.Name).ToArray());
                var syntax = SyntaxFactory.ParseStatement(body);
                var declaredMethod =
                    SyntaxFactory.MethodDeclaration(SyntaxFactory.ParseTypeName(method.ReturnType.GetCSTypeName()), method.Name)
                        .AddParameterListParameters(parameters.Select(t =>
                                SyntaxFactory.Parameter(SyntaxFactory.Identifier(t.Name))
                                .WithType(SyntaxFactory.ParseTypeName(t.ParameterType.FullName))).ToArray()
                                )
                        .AddModifiers(SyntaxFactory.Token(SyntaxKind.PublicKeyword))
                        .WithBody(SyntaxFactory.Block(syntax));

                _methods.Add(declaredMethod);
            }
            return this;
        }
        public string GenerateCode()
        {
            // Add the field, the property and method to the class.
            _classDeclarationSyntax = _classDeclarationSyntax.AddMembers(_fieldDeclarationSyntaxList.ToArray());
            _classDeclarationSyntax = _classDeclarationSyntax.AddMembers(_constructorDeclarationSyntax);
            _classDeclarationSyntax = _classDeclarationSyntax.AddMembers(_properties.ToArray());
            _classDeclarationSyntax = _classDeclarationSyntax.AddMembers(_methods.ToArray());

            // Add the class to the namespace.
            _namespaceDeclarationSyntax = _namespaceDeclarationSyntax.AddMembers(_classDeclarationSyntax);

            // Normalize and get code as string.
            var code = _namespaceDeclarationSyntax
                .NormalizeWhitespace()
                .ToFullString();
            return code;
        }
    }
}
