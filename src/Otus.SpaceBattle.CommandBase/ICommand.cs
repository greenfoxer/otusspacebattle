﻿namespace Otus.SpaceBattle.CommandBase
{
    public interface ICommand
    {
        void Execute();
    }
}