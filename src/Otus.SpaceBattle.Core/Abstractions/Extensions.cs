﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace Otus.SpaceBattle.Core.Abstractions
{
    public static class Extensions
    {
        public static bool SetPropertyOrThrows( this IUObject _object, string prop, object value)
        {
            var result = _object.SetProperty(prop, value);
            return result ? result : throw new InvalidOperationException(prop);
        }
        public static T GetPropertyOrThrows<T>(this IUObject _object, string prop)
        {
            try
            {
                return (T)_object.GetProperty(prop);
            }
            catch (NullReferenceException)
            {
                throw new MissingFieldException(prop);
            }
        }
        /// <summary>
        /// Метод расширения, преобразующий Vector2 из декартовых координат в полярные
        ///
        /// </summary>
        /// <param name="vector"></param>
        /// <param name="maxDirections">максимальное количество сегментов</param>
        /// <returns> Возвращает кортеж значений {модуль;сегмент} </returns>
        internal static Tuple<int,int> Polarize(this Vector2 vector, int maxDirections)
        {
            int module = (int)Math.Ceiling(Math.Sqrt(vector.X * vector.X + vector.Y * vector.Y));
            double tan = vector.Y / vector.X;
            double delta = double.PositiveInfinity;
            int angle = 0;
            for(int i =0; i< maxDirections; i++)
            {
                var current_delta = Math.Abs(tan - Math.Tan(2 * Math.PI * i / maxDirections));
                if(current_delta < delta)
                {
                    angle = i;
                    delta = current_delta;
                }
            }    
            var result = new Tuple<int,int>(module,angle);
            return result;
        }
    }
}
