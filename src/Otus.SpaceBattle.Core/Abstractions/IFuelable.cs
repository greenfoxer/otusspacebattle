﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Otus.SpaceBattle.Core.Abstractions
{
    public interface IFuelable
    {
        int Fuel { get; set; }
        int Сonsumption { get; set; }
    }
}
