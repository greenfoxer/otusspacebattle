﻿using System.Numerics;

namespace Otus.SpaceBattle.Core.Abstractions
{
    public interface IMovable
    {
        Vector2 Position { get; set; }
        Vector2 Velocity { get; set; }
    }
}
