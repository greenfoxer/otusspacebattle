﻿namespace Otus.SpaceBattle.Core.Abstractions
{
    public interface IRotable
    {
        int Direction { get; set; }
        int AngularVelocity { get; set; }
    }
}
