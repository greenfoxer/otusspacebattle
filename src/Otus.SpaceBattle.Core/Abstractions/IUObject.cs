﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Otus.SpaceBattle.Core.Abstractions
{
    public interface IUObject
    {
        object GetProperty(string prop);
        bool SetProperty(string prop, object value);
    }
}
