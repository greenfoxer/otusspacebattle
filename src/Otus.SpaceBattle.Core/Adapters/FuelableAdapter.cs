﻿using Otus.SpaceBattle.Core.Abstractions;

namespace Otus.SpaceBattle.Core.Adapters
{
    public class FuelableAdapter : IFuelable
    {
        IUObject _object;

        public FuelableAdapter(IUObject obj)
        {
            this._object = obj;
        }
        public int Fuel { 
            get => _object.GetPropertyOrThrows<int>("Fuel"); 
            set => _object.SetPropertyOrThrows("Fuel", value); 
        }
        public int Сonsumption
        {
            get => _object.GetPropertyOrThrows<int>("Сonsumption");
            set => _object.SetPropertyOrThrows("Сonsumption", value); }
    }
}
