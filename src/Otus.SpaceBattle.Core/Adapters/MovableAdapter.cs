﻿using Otus.SpaceBattle.Core.Abstractions;
using System.Numerics;

namespace Otus.SpaceBattle.Core.Adapters
{
    public class MovableAdapter : IMovable
    {
        IUObject _object;
        public MovableAdapter(IUObject obj)
        {
            this._object = obj;
        }
        public Vector2 Position 
        { 
            get => _object.GetPropertyOrThrows<Vector2>("Position"); 
            set => _object.SetPropertyOrThrows("Position", value); 
        }
        public Vector2 Velocity
        {
            get
            {
                var velocity = _object.GetPropertyOrThrows<int>("Velocity");
                var maxDirection = _object.GetPropertyOrThrows<int>("MaxDirections");
                var direction = _object.GetPropertyOrThrows<int>("Direction");
                
                return new Vector2(
                (int)(velocity * Math.Cos(2 * Math.PI * direction / maxDirection)),
                (int)(velocity * Math.Sin(2 * Math.PI * direction / maxDirection))
                ); 
            }
            set
            {
                var polar_coords = value.Polarize(_object.GetPropertyOrThrows<int>("MaxDirections"));
                _object.SetProperty("Velocity", polar_coords.Item1);
                _object.SetProperty("Direction", polar_coords.Item2);
            }
        }
    }
}
