﻿using Otus.SpaceBattle.Core.Abstractions;

namespace Otus.SpaceBattle.Core.Adapters
{
    public class RotateAdapter : IRotable
    {
        IUObject _object;

        public RotateAdapter(IUObject obj)
        {
            this._object = obj;
        }

        public int Direction
        {
            get => _object.GetPropertyOrThrows<int>("Direction");
            set
            {
                int maxDirections = _object.GetPropertyOrThrows<int>("MaxDirections");
                var rawDirection = value % maxDirections;
                _object.SetPropertyOrThrows("Direction", rawDirection >= 0 ? rawDirection : maxDirections + rawDirection);
            }
        }
        public int AngularVelocity 
        {
            get => _object.GetPropertyOrThrows<int>("AngularVelocity");
            set => _object.SetPropertyOrThrows("AngularVelocity",value);
        }
    }
}
