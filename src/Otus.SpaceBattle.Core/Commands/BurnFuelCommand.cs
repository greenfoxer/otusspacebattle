﻿using Otus.SpaceBattle.Core.Abstractions;

namespace Otus.SpaceBattle.Core.Commands
{
    public class BurnFuelCommand : ICommand
    {
        private IFuelable _fuelable;

        public BurnFuelCommand(IFuelable fuelable)
        {
            this._fuelable = fuelable;
        }
        public void Execute()
        {
            _fuelable.Fuel -= _fuelable.Сonsumption;
        }
    }
}
