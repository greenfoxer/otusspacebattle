﻿using Otus.SpaceBattle.Core.Abstractions;
using Otus.SpaceBattle.Core.Exceptions;

namespace Otus.SpaceBattle.Core.Commands
{
    public class CheckFuelCommand : ICommand
    {
        private IFuelable _fuelable;

        public CheckFuelCommand(IFuelable fuelable)
        {
            this._fuelable = fuelable;
        }

        public void Execute()
        {
            if (_fuelable.Сonsumption > _fuelable.Fuel)
                throw new CommandException("Not enough fuel!");
        }
    }
}
