﻿using Otus.SpaceBattle.Core.Abstractions;

namespace Otus.SpaceBattle.Core.Commands
{
    public class MoveCommand : ICommand
    {
        private IMovable _object;

        public MoveCommand(IMovable _object)
        {
            this._object = _object as IMovable;
        }

        public void Execute()
        {
            _object.Position += _object.Velocity;
        }
    }
}
