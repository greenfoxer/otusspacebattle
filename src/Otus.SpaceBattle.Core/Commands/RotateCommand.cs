﻿using Otus.SpaceBattle.Core.Abstractions;

namespace Otus.SpaceBattle.Core.Commands
{
    public class RotateCommand : ICommand
    {
        private IRotable _object;

        public RotateCommand(IRotable obj)
        {
            this._object = obj;
        } 
        public void Execute()
        {
            this._object.Direction += this._object.AngularVelocity;
        }
    }
}
