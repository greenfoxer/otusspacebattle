﻿namespace Otus.SpaceBattle.IoC.Abstractions
{
    public interface IIoCExecutionStrategy
    {
        void SetData(object obj, object[] args);
        bool CheckStrategy<T>();
        object Resolve<T>();
    }
}
