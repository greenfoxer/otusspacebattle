﻿namespace Otus.SpaceBattle.IoC.Abstractions
{
    public interface IScope 
    {
        void Add(string key, object value);
        void Remove(string key);
        object this[string key] { get; }
        string Name { get; }
    }
}
