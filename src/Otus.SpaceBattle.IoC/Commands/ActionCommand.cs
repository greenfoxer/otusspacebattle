﻿using Otus.SpaceBattle.CommandBase;

namespace Otus.SpaceBattle.IoC.Commands
{
    public class ActionCommand : ICommand
    {
        private object _action;
        private object[] _args;

        public ActionCommand(object action, object[] args = null)
        {
            this._action = action;
            this._args = args;
        }


        public void Execute()
        {
            if (_args != null)
                (_action as Action<object[]>).Invoke(_args);
            else
                (_action as Action).Invoke();
        }
    }
}