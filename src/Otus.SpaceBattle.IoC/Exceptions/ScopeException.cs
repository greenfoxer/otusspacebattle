﻿namespace Otus.SpaceBattle.IoC.Exceptions
{
    public class ScopeException : Exception
    {
        public ScopeException(string error) : base(error)
        { }
        public ScopeException(string message, Exception innerException) : base(message, innerException)
        { }
    }
}
