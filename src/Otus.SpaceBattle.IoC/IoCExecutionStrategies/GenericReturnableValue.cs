﻿using Otus.SpaceBattle.IoC.Abstractions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Otus.SpaceBattle.IoC.IoCExecutionStrategies
{
    public class GenericReturnableValue : IIoCExecutionStrategy
    {
        object _result;
        private object _obj;
        private object[] _args;
        public bool CheckStrategy<T>()
        {
            if (_obj is Func<object[], object>)
            {
                var result = ((Func<object[], object>)_obj).Invoke(_args);
                if (result is T) {
                    _result = (T)result;
                    return true;
                }
            }
            return false;
        }

        public object Resolve<T>()
        {
            return _result;
        }

        public void SetData(object obj, object[] args)
        {
            _obj = obj;
            _args = args;
        }
    }
}
