﻿using Otus.SpaceBattle.CommandBase;
using Otus.SpaceBattle.IoC.Abstractions;
using Otus.SpaceBattle.IoC.Commands;

namespace Otus.SpaceBattle.IoC.IoCExecutionStrategies
{
    public class NonParametrizedActionToCommandStrategy : IIoCExecutionStrategy
    {

        private object _obj;
        public bool CheckStrategy<T>()
        {
            return _obj is Action && typeof(T) == typeof(ICommand);
        }

        public object Resolve<T>()
        {
            return new ActionCommand(_obj);
        }
        public void SetData(object obj, object[] args = null)
        {
            _obj = obj;
        }
    }
}
