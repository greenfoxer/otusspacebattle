﻿using Otus.SpaceBattle.IoC.Abstractions;

namespace Otus.SpaceBattle.IoC.IoCExecutionStrategies
{
    public class NonParametrizedLambdaStrategy : IIoCExecutionStrategy
    {
        private object _obj;
        public bool CheckStrategy<T>()
        {
            return _obj.GetType() == typeof(Func<T>);
        }

        public object Resolve<T>()
        {
            return (_obj as Func<T>).Invoke();
        }


        public void SetData(object obj, object[] args = null)
        {
            _obj = obj;
        }
    }
}
