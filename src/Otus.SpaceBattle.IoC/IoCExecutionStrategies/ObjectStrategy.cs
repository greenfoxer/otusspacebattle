﻿using Otus.SpaceBattle.IoC.Abstractions;

namespace Otus.SpaceBattle.IoC.IoCExecutionStrategies
{
    public class ObjectStrategy : IIoCExecutionStrategy
    {
        private object _obj;
        public bool CheckStrategy<T>()
        {
            return _obj is T;
        }

        public object Resolve<T>()
        {
            return (T)_obj;
        }

        public void SetData(object obj, object[] args)
        {
            _obj = obj;
        }
    }
}
