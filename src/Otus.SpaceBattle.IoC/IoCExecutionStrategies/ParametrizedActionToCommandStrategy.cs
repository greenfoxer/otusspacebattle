﻿using Otus.SpaceBattle.CommandBase;
using Otus.SpaceBattle.IoC.Abstractions;
using Otus.SpaceBattle.IoC.Commands;

namespace Otus.SpaceBattle.IoC.IoCExecutionStrategies
{
    public class ParametrizedActionToCommandStrategy : IIoCExecutionStrategy
    {

        private object _obj;
        private object[] _args;
        public bool CheckStrategy<T>()
        {
            return _obj is Action<object[]> && typeof(T) == typeof(ICommand);
        }

        public object Resolve<T>()
        {
            return new ActionCommand(_obj,_args);
        }
        public void SetData(object obj, object[] args = null)
        {
            _obj = obj;
            _args = args;
        }
    }
}
