﻿using Otus.SpaceBattle.IoC.Abstractions;

namespace Otus.SpaceBattle.IoC.IoCExecutionStrategies
{
    public class ParametrizedLambdaStrategy : IIoCExecutionStrategy
    {

        private object _obj;
        private object[] _args;

        public bool CheckStrategy<T>()
        {
            return _obj is Func<object[],T>;
        }

        public object Resolve<T>()
        {
            return (_obj as Func<object[], T>).Invoke(_args);
        }

        public void SetData(object obj, object[] args)
        {
            _obj = obj;
            _args = args;
        }
    }
}
