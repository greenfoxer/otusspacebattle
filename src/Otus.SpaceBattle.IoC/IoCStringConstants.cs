﻿namespace Otus.SpaceBattle.IoC
{
    public static class IoCStringConstants
    {
        public static string Register => "IoC.Register";
        public static string GlobalCommandsQueue => "GlobalCommandQueue";
        public static string GlobalCommandsQueueAdd => "GlobalCommandQueueAdd";
        public static string DefaultScopeName => "root";
    }
}
