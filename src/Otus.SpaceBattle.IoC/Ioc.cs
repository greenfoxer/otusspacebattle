﻿using Otus.SpaceBattle.CommandBase;
using Otus.SpaceBattle.IoC.Abstractions;
using Otus.SpaceBattle.IoC.Exceptions;
using Otus.SpaceBattle.IoC.IoCExecutionStrategies;
using Otus.SpaceBattle.IoC.Realisations;
using System.Collections.Concurrent;

namespace Otus.SpaceBattle.IoC
{
    public static partial class IoCContainer
    {
        internal class RegisterCommand : ICommand
        {
            private string _key;
            private object _value;

            public RegisterCommand(string key, object value)
            {
                this._key = key;
                this._value = value; 
            }
            public void Execute()
            {
                ScopeManager.GetCurrentScope().Add(_key, _value);
            }
        }
        private static ConcurrentBag<IIoCExecutionStrategy> _casts = new ConcurrentBag<IIoCExecutionStrategy>();
        static IoCContainer()
        {
            // возможные страндартные стратегии каста объекта из контейнера к конечному значению
            _casts.Add(new ParametrizedLambdaStrategy());
            _casts.Add(new NonParametrizedLambdaStrategy());
            _casts.Add(new ObjectStrategy()); 
            // в целях демонстрации данные стратегии добавляютс в тестах
            _casts.Add(new NonParametrizedActionToCommandStrategy());
            _casts.Add(new ParametrizedActionToCommandStrategy());
            _casts.Add(new GenericReturnableValue());

            Func<object[], ICommand> register = (args) => { return new RegisterCommand((string)args[0], args[1]); };
            ScopeManager.GetScopeByName(IoCStringConstants.DefaultScopeName).Add(IoCStringConstants.Register, register );
        }
        public static void RegisterStrategy(IIoCExecutionStrategy strategy)
        {
            if (!_casts.Select(t => t.GetType()).Contains(strategy.GetType()))
                _casts.Add(strategy);
            else
                throw new IoCException($"Strategy of {strategy.GetType().Name} already exists");
        }

        public static T Resolve<T>(string key, params object[] args)
        {
            if (string.IsNullOrEmpty(key))
                throw new IoCException("Key cannot be null or empty");

            var currentScope = ScopeManager.GetCurrentScope();
            object dependency;

            try
            {
                dependency = currentScope[key];
            }
            catch(ScopeException ex)
            {
                throw new IoCException(ex.Message, ex);
            }

            foreach(var cast in _casts)
            {
                cast.SetData(dependency, args);
                if (cast.CheckStrategy<T>())
                    return (T) cast.Resolve<T>();
            }    

            throw new IoCException($"Can't find resolver for {key}!");

            //if (dependency.GetType() == typeof(Func<object[], T>))
            //    return (T)(dependency as Func<object[], T>).Invoke(args);
            //else if (dependency.GetType() == typeof(Func<T>))
            //    return (T)(dependency as Func<T>).Invoke();
            //else
            //    return (T)dependency;
        }
    }
}