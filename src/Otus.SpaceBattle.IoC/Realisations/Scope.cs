﻿using Otus.SpaceBattle.IoC.Abstractions;
using Otus.SpaceBattle.IoC.Exceptions;
using System.Collections.Concurrent;

namespace Otus.SpaceBattle.IoC.Realisations
{
    public class Scope : IScope
    {
        public string Name { get; }
        private IScope _parentScope;

        ConcurrentDictionary<string, object> _scopeData;

        public Scope(string name, IScope parent = null)
        {
            this.Name = name;
            this._parentScope = parent;
            this._scopeData = new ConcurrentDictionary<string, object>();
        }

        public void Add(string key, object value)
        {
            if (string.IsNullOrEmpty(key))
                throw new ScopeException("Dependency key could not be null or empty!");
            if (_scopeData.ContainsKey(key))
                _scopeData[key] = value;
                //throw new ScopeException($"Scope {Name} already contain dependency {key}");
            else if (!_scopeData.TryAdd(key, value))
                throw new ScopeException($"Fail adding dependency {key} to scope {Name}");
        }

        public void Remove(string key)
        {
            if (!_scopeData.TryRemove(key, out var value))
                throw new ScopeException($"Fail when removing dependency {key} from scope {Name}");
        }
        public object this[string key] {
            get
            {
                if (_scopeData.TryGetValue(key, out var value))
                    return value;
                else if (_parentScope != null)
                    return _parentScope[key];

                throw new ScopeException($"Fail when reading dependency {key} from scope {Name}");
            }
        }
    }
}
