﻿using Otus.SpaceBattle.IoC.Abstractions;

namespace Otus.SpaceBattle.IoC.Realisations
{
    public static class ScopeManager
    {
        private static IScope _defaultScope = new Scope(IoCStringConstants.DefaultScopeName);
        public static IScope DefaultScope => _defaultScope;
        private static Dictionary<string, IScope> _scopes = new Dictionary<string, IScope>() { { IoCStringConstants.DefaultScopeName, DefaultScope } };
        private static ThreadLocal<IScope> _scope = new ThreadLocal<IScope>();  
        public static IScope GetScopeByName(string name)
        {
            return _scopes[name];
        }
        public static void SetCurrentScope(IScope scope)
        {
            _scope.Value = scope;
        }
        public static IScope GetCurrentScope()
        {
            if (_scope.Value == null)
                _scope.Value = new Scope($"Thread.{Thread.CurrentThread.ManagedThreadId}", DefaultScope);
            return _scope.Value;
        }
        static public IScope CreateNew(string name, IScope parent = null)
        {
            IScope scope = new Scope(name, parent ?? DefaultScope);
            _scopes.Add(name, scope);
            return scope;
        }
    }
}
