﻿namespace Otus.SpaceBattle.Tests
{
    public class A
    {
        static int _counter = 1;
        public int Number => _counter++;
    }
    public class AAA
    {
        public DateTime Date { get; set; }
        public string Name { get; set; }
        public int Number { get; set; }
        public AAA(DateTime date, string name, int number)
        {
            Date = date;
            Name = name;
            Number = number;
        }
        public AAA(DateTime date, string name)
        {
            Date = date;
            Name = name;
        }
        public AAA(DateTime date,  int number)
        {
            Date = date;
            Number = number;
        }
        public AAA( string name, int number)
        {
            Name = name;
            Number = number;
        }
    }
}
