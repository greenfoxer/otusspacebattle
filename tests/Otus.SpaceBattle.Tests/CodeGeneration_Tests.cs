﻿using Newtonsoft.Json.Linq;
using Otus.SpaceBattle.CodeGenerator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Otus.SpaceBattle.Tests
{
    public interface IAdapterInterface
    {
        int PropertyValue { get; set; }
        void SomeMethod();
        void MethodWithParameter(int val);
    }
    public class CodeGeneration_Tests
    {
        
        [Fact]
        public void Auto_generate_movable_adapter_and_execute_move_command_on_them()
        {
            var generator = new AdapterGenerator(typeof(IMovable));

            var result = generator.GetCompiledType();

            IoCContainer.Resolve<ICommand>(IoCStringConstants.Register, "IMovable.Position.Get", ((object[] args) => { return ((IUObject)args[0]).GetPropertyOrThrows<Vector2>("Position"); })).Execute();
            IoCContainer.Resolve<ICommand>(IoCStringConstants.Register, "IMovable.Velocity.Get", ((object[] args) => {
                var _object = ((IUObject)args[0]);
                var velocity = _object.GetPropertyOrThrows<int>("Velocity");
                var maxDirection = _object.GetPropertyOrThrows<int>("MaxDirections");
                var direction = _object.GetPropertyOrThrows<int>("Direction");

                return new Vector2(
                (int)(velocity * Math.Cos(2 * Math.PI * direction / maxDirection)),
                (int)(velocity * Math.Sin(2 * Math.PI * direction / maxDirection))
                );
            })).Execute();
            IoCContainer.Resolve<ICommand>(IoCStringConstants.Register, "IMovable.Position.Set", ((object[] args) => { return new SetupCommand((IUObject)args[0], "Position", args[1]); })).Execute();

            Dictionary<string, object> data = new Dictionary<string, object>() {
                { "Position", new Vector2(12, 5) },
                { "Velocity", 8 },
                { "MaxDirections", 72 },
                { "Direction", 31 }
            };


            var obj = new UobjectMoq(data);
            var value = Activator.CreateInstance(result, obj);
            var command = new MoveCommand((IMovable)value);
            command.Execute();


            data["Position"].Should().Be(new Vector2(5, 8));
        }
        [Fact]
        public void Calling_adapter_creator_via_IOCContainer()
        {
            IoCContainer.Resolve<ICommand>(IoCStringConstants.Register, "Adapter", (object[] args) => {
                var generator = new AdapterGenerator((Type)args[0]);
                var @type = generator.GetCompiledType();
                return Activator.CreateInstance(@type, (IUObject)args[1]);
            }).Execute();

            IoCContainer.Resolve<ICommand>(IoCStringConstants.Register, "IMovable.Position.Get", ((object[] args) => { return ((IUObject)args[0]).GetPropertyOrThrows<Vector2>("Position"); })).Execute();
            IoCContainer.Resolve<ICommand>(IoCStringConstants.Register, "IMovable.Velocity.Get", ((object[] args) => {
                var _object = ((IUObject)args[0]);
                var velocity = _object.GetPropertyOrThrows<int>("Velocity");
                var maxDirection = _object.GetPropertyOrThrows<int>("MaxDirections");
                var direction = _object.GetPropertyOrThrows<int>("Direction");

                return new Vector2(
                (int)(velocity * Math.Cos(2 * Math.PI * direction / maxDirection)),
                (int)(velocity * Math.Sin(2 * Math.PI * direction / maxDirection))
                );
            })).Execute();
            IoCContainer.Resolve<ICommand>(IoCStringConstants.Register, "IMovable.Position.Set", ((object[] args) => { return new SetupCommand((IUObject)args[0], "Position", args[1]); })).Execute();

            Dictionary<string, object> data = new Dictionary<string, object>() {
                { "Position", new Vector2(12, 5) },
                { "Velocity", 8 },
                { "MaxDirections", 72 },
                { "Direction", 31 }
            };


            var obj = new UobjectMoq(data);
            var adapter = IoCContainer.Resolve<IMovable>("Adapter", typeof(IMovable), obj);

            var command = new MoveCommand((IMovable)adapter);
            command.Execute();


            data["Position"].Should().Be(new Vector2(5, 8));
        } 
    
        [Fact]
        public void Auto_generate_few_adapters_and_execute_move_macrocommand_on_them()
        {
            IoCContainer.Resolve<ICommand>(IoCStringConstants.Register, "Adapter",(object[] args) => {
                var generator = new AdapterGenerator((Type)args[0]);
                var @type = generator.GetCompiledType();
                return Activator.CreateInstance(@type, (IUObject)args[1]);
            }).Execute();

            IoCContainer.Resolve<ICommand>(IoCStringConstants.Register, "IMovable.Position.Get", ((object[] args) => { return ((IUObject)args[0]).GetPropertyOrThrows<Vector2>("Position"); })).Execute();
            IoCContainer.Resolve<ICommand>(IoCStringConstants.Register, "IMovable.Velocity.Get", ((object[] args) => {
                var _object = ((IUObject)args[0]);
                var velocity = _object.GetPropertyOrThrows<int>("Velocity");
                var maxDirection = _object.GetPropertyOrThrows<int>("MaxDirections");
                var direction = _object.GetPropertyOrThrows<int>("Direction");

                return new Vector2(
                (int)(velocity * Math.Cos(2 * Math.PI * direction / maxDirection)),
                (int)(velocity * Math.Sin(2 * Math.PI * direction / maxDirection))
                );
            })).Execute();
            IoCContainer.Resolve<ICommand>(IoCStringConstants.Register, "IMovable.Position.Set", ((object[] args) => { return new SetupCommand((IUObject)args[0], "Position", args[1]); })).Execute();

            IoCContainer.Resolve<ICommand>(IoCStringConstants.Register, "IFuelable.Сonsumption.Get", ((object[] args) => { return ((IUObject)args[0]).GetPropertyOrThrows<int>("Сonsumption"); })).Execute();
            IoCContainer.Resolve<ICommand>(IoCStringConstants.Register, "IFuelable.Fuel.Get", ((object[] args) => { return ((IUObject)args[0]).GetPropertyOrThrows<int>("Fuel"); })).Execute();
            IoCContainer.Resolve<ICommand>(IoCStringConstants.Register, "IFuelable.Fuel.Set", ((object[] args) => { return new SetupCommand((IUObject)args[0], "Fuel", args[1]); })).Execute();

            IoCContainer.Resolve<ICommand>(IoCStringConstants.Register, "IRotable.AngularVelocity.Get", ((object[] args) => { return ((IUObject)args[0]).GetPropertyOrThrows<int>("AngularVelocity"); })).Execute();
            IoCContainer.Resolve<ICommand>(IoCStringConstants.Register, "IRotable.Direction.Get", ((object[] args) => { return ((IUObject)args[0]).GetPropertyOrThrows<int>("Direction"); })).Execute();
            IoCContainer.Resolve<ICommand>(IoCStringConstants.Register, "IRotable.Direction.Set", ((object[] args) => {
                int maxDirections = ((IUObject)args[0]).GetPropertyOrThrows<int>("MaxDirections");
                var rawDirection = ((int)args[1]) % maxDirections;
                ((IUObject)args[0]).SetPropertyOrThrows("Direction", rawDirection >= 0 ? rawDirection : maxDirections + rawDirection);
            })).Execute();


            Dictionary<string, object> data = new Dictionary<string, object>() {
                    { "Position", new Vector2(12, 5) },
                    { "Velocity", 8 },
                    { "MaxDirections", 72 },
                    { "Direction", 31 },
                    { "AngularVelocity", 5 },
                    { "Сonsumption", 5 },
                    { "Fuel", 20 }
                };


            var obj = new UobjectMoq(data);

            var fuelable = IoCContainer.Resolve<IFuelable>("Adapter", typeof(IFuelable), obj);
            var movable = IoCContainer.Resolve<IMovable>("Adapter", typeof(IMovable), obj); 
            var rotable = IoCContainer.Resolve<IRotable>("Adapter", typeof(IRotable), obj); 

            var checkFuelCmd = new CheckFuelCommand(fuelable);
            var burnFuelCmd = new BurnFuelCommand(fuelable);
            var moveCmd = new MoveCommand(movable);
            var rotateCmd = new RotateCommand(rotable);

            var sut = new LinearMacrocommand(checkFuelCmd, burnFuelCmd, rotateCmd, moveCmd);

            //Act 
            sut.Execute();

            //Assert
            data["Position"].Should().Be(new Vector2(4, 5));
            data["Direction"].Should().Be(36);
        }


        [Fact]
        public void Auto_generate_test_adapter_with_custom_methods()
        { 
            IoCContainer.Resolve<ICommand>(IoCStringConstants.Register, "Adapter", (object[] args) =>
            {
                var generator = new AdapterGenerator((Type)args[0], "Otus.SpaceBattle.Tests");
                var @type = generator.GetCompiledType();
                return Activator.CreateInstance(@type, (IUObject)args[1]);
            }).Execute();
            var mock = new Mock<IUObject>();
            int changeableValue = 0;
            IoCContainer.Resolve<ICommand>(IoCStringConstants.Register, "IAdapterInterface.SomeMethod", () => { changeableValue = 100; }).Execute();
            IoCContainer.Resolve<ICommand>(IoCStringConstants.Register, "IAdapterInterface.MethodWithParameter", (object[] args) => { changeableValue = (int)args[1]; }).Execute();

            var testAdapter = IoCContainer.Resolve<IAdapterInterface>("Adapter", typeof(IAdapterInterface), mock.Object);
            testAdapter.SomeMethod();
            changeableValue.Should().Be(100);

            testAdapter.MethodWithParameter(200);
            changeableValue.Should().Be(200);
        }
    }
}
