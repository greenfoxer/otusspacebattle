namespace Otus.SpaceBattle.Tests
{
    public class Commands_Tests
    {
        [Fact]
        public void Moving_object_from_one_point_to_another()
        {
            //Arrange
            var mock = new Mock<IMovable>();
            mock.SetupProperty<Vector2>(x => x.Position, new Vector2(12, 5));
            mock.SetupProperty<Vector2>(x => x.Velocity, new Vector2(-7, 3));
            var movable = mock.Object;
            var sut = new MoveCommand(movable);

            //Act
            sut.Execute();

            //Assert
            movable.Position.Should().Be(new Vector2(5, 8));
        }
        [Fact]
        public void When_command_fails_then_log_information_about_command()
        {
            //Arrange
            Queue<ICommand> queue = new Queue<ICommand>();
            var uobject = new Mock<IUObject>();
            uobject.Setup(x => x.GetProperty("Position")).Returns(null);
            var movable = new MovableAdapter(uobject.Object);
            var command = new MoveCommand(movable);
            queue.Enqueue(command);
            IExceptionHandler sut = new ExceptionHandler();
            //условный накопитель лога, который в будущем, наверное, надо брать из IoC
            List<string> errors = new List<string>();

            sut.Setup(typeof(MoveCommand),
                typeof(MissingFieldException),
                (cmd, ex) => queue.Enqueue(new LogCommand(cmd, ex, errors)));

            //Act
            while (queue.Count > 0)
            {
                var currentCommand = queue.Dequeue();
                try
                {
                    currentCommand.Execute();
                }
                catch (Exception ex)
                {
                    sut.Handle(currentCommand, ex);
                    ex.Should().BeOfType<MissingFieldException>();
                }
            }

            //Assert
            errors.Should().ContainEquivalentOf("Otus.SpaceBattle.Core.Commands.MoveCommand fails with message Position");
        }
        [Fact]
        public void When_command_fails_then_schedule_repeat_command_once()
        {
            //Arrange
            Queue<ICommand> queue = new Queue<ICommand>();
            var uobject = new Mock<IUObject>();
            uobject.Setup(x => x.GetProperty("Position")).Returns(null);
            var movable = new MovableAdapter(uobject.Object);
            var command = new MoveCommand(movable);
            queue.Enqueue(command);
            IExceptionHandler sut = new ExceptionHandler();
            //условный накопитель лога, который в будущем, наверное, надо брать из IoC
            List<string> errors = new List<string>();

            sut.Setup(typeof(MoveCommand),
                typeof(MissingFieldException),
                (cmd, ex) => queue.Enqueue(new Repeat1TimeCommand(cmd, ex)));
            sut.Setup(typeof(Repeat1TimeCommand),
                typeof(MissingFieldException),
                (cmd, ex) => queue.Enqueue(new LogCommand(cmd, ex, errors)));

            //Act
            while (queue.Count > 0)
            {
                var currentCommand = queue.Dequeue();
                try
                {
                    currentCommand.Execute();
                }
                catch (Exception ex)
                {
                    sut.Handle(currentCommand, ex);
                    ex.Should().BeOfType<MissingFieldException>();
                }
            }

            //Assert
            errors.Should().ContainEquivalentOf("Otus.SpaceBattle.Core.Commands.MoveCommand fails with message Position");
        }
        [Fact]
        public void When_command_fails_then_schedule_repeat_command_twice()
        {
            //Arrange
            Queue<ICommand> queue = new Queue<ICommand>();
            var uobject = new Mock<IUObject>();
            uobject.Setup(x => x.GetProperty("Position")).Returns(null);
            var movable = new MovableAdapter(uobject.Object);
            var command = new MoveCommand(movable);
            queue.Enqueue(command);
            IExceptionHandler sut = new ExceptionHandler();
            //условный накопитель лога, который в будущем, наверное, надо брать из IoC
            List<string> errors = new List<string>();

            sut.Setup(typeof(MoveCommand),
                typeof(MissingFieldException),
                (cmd, ex) => queue.Enqueue(new Repeat2TimesCommand(cmd, ex)));
            sut.Setup(typeof(Repeat2TimesCommand),
                typeof(MissingFieldException),
                (cmd, ex) => queue.Enqueue(new Repeat1TimeCommand(cmd, ex)));
            sut.Setup(typeof(Repeat1TimeCommand),
                typeof(MissingFieldException),
                (cmd, ex) => queue.Enqueue(new LogCommand(cmd, ex, errors)));

            //Act
            while (queue.Count > 0)
            {
                var currentCommand = queue.Dequeue();
                try
                {
                    currentCommand.Execute();
                }
                catch (Exception ex)
                {
                    sut.Handle(currentCommand, ex);
                    ex.Should().BeOfType<MissingFieldException>();
                }
            }

            //Assert
            errors.Should().ContainEquivalentOf("Otus.SpaceBattle.Core.Commands.MoveCommand fails with message Position");
        }
    }
}