﻿using Otus.SpaceBattle.Application.EventLoop;
using Otus.SpaceBattle.CodeGenerator;
using Otus.SpaceBattle.IoC.Commands;
using Otus.SpaceBattle.IoC.Realisations;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json.Serialization.Metadata;
using System.Threading;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Otus.SpaceBattle.Tests
{
    public class EventLoop_Test
    {
        [Fact]
        public void Single_event_loop_works()
        {
            var globalQueue = new BlockingCollection<ICommand>();
            Barrier barrier = new Barrier(2);

            ScopeManager.SetCurrentScope(ScopeManager.DefaultScope);

            IoCContainer.Resolve<ICommand>(IoCStringConstants.Register, IoCStringConstants.GlobalCommandsQueue, globalQueue).Execute();
            IoCContainer.Resolve<ICommand>(IoCStringConstants.Register, IoCStringConstants.GlobalCommandsQueueAdd, (object[] args) =>
            {
                IoCContainer.Resolve<BlockingCollection<ICommand>>(IoCStringConstants.GlobalCommandsQueue).Add((ICommand)args[0]);
            } ).Execute();

            IoCContainer.Resolve<ICommand>(IoCStringConstants.Register, "CreateNewEventLoop", (object[] args) => {
                EventLoopManager.StartEventLoop((string)args[0]);
            }).Execute();

            IoCContainer.Resolve<ICommand>(IoCStringConstants.Register, "Adapter", (object[] args) => {
                var generator = new AdapterGenerator((Type)args[0]);
                var @type = generator.GetCompiledType();
                return Activator.CreateInstance(@type, (IUObject)args[1]);
            }).Execute();
            IoCContainer.Resolve<ICommand>(IoCStringConstants.Register, "IMovable.Position.Get", ((object[] args) => { return ((IUObject)args[0]).GetPropertyOrThrows<Vector2>("Position"); })).Execute();
            IoCContainer.Resolve<ICommand>(IoCStringConstants.Register, "IMovable.Velocity.Get", ((object[] args) => {
                var _object = ((IUObject)args[0]);
                var velocity = _object.GetPropertyOrThrows<int>("Velocity");
                var maxDirection = _object.GetPropertyOrThrows<int>("MaxDirections");
                var direction = _object.GetPropertyOrThrows<int>("Direction");
                return new Vector2(
                (int)(velocity * Math.Cos(2 * Math.PI * direction / maxDirection)),
                (int)(velocity * Math.Sin(2 * Math.PI * direction / maxDirection))
                );
            })).Execute();
            IoCContainer.Resolve<ICommand>(IoCStringConstants.Register, "IMovable.Position.Set", ((object[] args) => { return new SetupCommand((IUObject)args[0], "Position", args[1]); })).Execute();

            Dictionary<string, object> data = new Dictionary<string, object>() {
                { "Position", new Vector2(12, 5) },
                { "Velocity", 8 },
                { "MaxDirections", 72 },
                { "Direction", 31 }
            };

            var obj = new UobjectMoq(data);
            var adapter = IoCContainer.Resolve<IMovable>("Adapter", typeof(IMovable), obj);
            var command = new MoveCommand((IMovable)adapter);

            IoCContainer.Resolve<ICommand>(IoCStringConstants.GlobalCommandsQueueAdd, command).Execute();
            IoCContainer.Resolve<ICommand>(IoCStringConstants.GlobalCommandsQueueAdd, new ActionCommand(() => { barrier.SignalAndWait(); })).Execute();

            globalQueue.Should().HaveCount(2);
            
            IoCContainer.Resolve<ICommand>("CreateNewEventLoop","singleThread").Execute();

            barrier.SignalAndWait(); // ждем немного, чтобы команда точно выполнилась.

            globalQueue.Should().HaveCount(0);
            data["Position"].Should().Be(new Vector2(5, 8));
            
        }
        class ExecuteActionCommand : ICommand
        {
            private readonly Action<int> _action;
            private readonly int _value;
            Barrier barrier = new Barrier(2);

            public ExecuteActionCommand(Action<int> action, int value)
            {
                _action = action;
                _value = value;

            }
            public void Execute()
            {
                _action(_value);
            }
        }
        [Fact]
        public void Hard_stop_breaks_event_loop()
        {
            var globalQueue = new BlockingCollection<ICommand>();

            ScopeManager.SetCurrentScope(ScopeManager.DefaultScope);

            IoCContainer.Resolve<ICommand>(IoCStringConstants.Register, IoCStringConstants.GlobalCommandsQueue, globalQueue).Execute();
            IoCContainer.Resolve<ICommand>(IoCStringConstants.Register, IoCStringConstants.GlobalCommandsQueueAdd, (object[] args) =>
            {
                IoCContainer.Resolve<BlockingCollection<ICommand>>(IoCStringConstants.GlobalCommandsQueue).Add((ICommand)args[0]);
            }).Execute();
            IoCContainer.Resolve<ICommand>(IoCStringConstants.Register, "CreateNewEventLoop", (object[] args) => {
                EventLoopManager.StartEventLoop((string)args[0]);
            }).Execute();

            int changeableValue = 10;
            var action = (int a) => { changeableValue = a; };

            IoCContainer.Resolve<ICommand>(IoCStringConstants.GlobalCommandsQueueAdd, new ExecuteActionCommand(action, 100)).Execute();

            IoCContainer.Resolve<ICommand>(IoCStringConstants.GlobalCommandsQueueAdd, new HardStopCommand()).Execute();

            IoCContainer.Resolve<ICommand>(IoCStringConstants.GlobalCommandsQueueAdd, new ExecuteActionCommand(action, 200)).Execute();

            IoCContainer.Resolve<ICommand>("CreateNewEventLoop", "el").Execute();

            Thread.Sleep(1000);

            changeableValue.Should().Be(100);
            globalQueue.Should().HaveCount(1);
        }
        [Fact]
        public void Soft_stop_breaks_event_loop()
        {
            var globalQueue = new BlockingCollection<ICommand>();
            Barrier barrier = new Barrier(2);

            ScopeManager.SetCurrentScope(ScopeManager.DefaultScope);

            IoCContainer.Resolve<ICommand>(IoCStringConstants.Register, IoCStringConstants.GlobalCommandsQueue, globalQueue).Execute();
            IoCContainer.Resolve<ICommand>(IoCStringConstants.Register, IoCStringConstants.GlobalCommandsQueueAdd, (object[] args) =>
            {
                IoCContainer.Resolve<BlockingCollection<ICommand>>(IoCStringConstants.GlobalCommandsQueue).Add((ICommand)args[0]);
            }).Execute();
            IoCContainer.Resolve<ICommand>(IoCStringConstants.Register, "CreateNewEventLoop", (object[] args) => {
                EventLoopManager.StartEventLoop((string)args[0]);
            }).Execute();

            int changeableValue = 10;
            var action = (int a) => { changeableValue = a; };
            IoCContainer.Resolve<ICommand>(IoCStringConstants.GlobalCommandsQueueAdd, new ExecuteActionCommand(action, 100)).Execute();

            IoCContainer.Resolve<ICommand>(IoCStringConstants.GlobalCommandsQueueAdd, new SoftStopCommand()).Execute();

            IoCContainer.Resolve<ICommand>(IoCStringConstants.GlobalCommandsQueueAdd, new ExecuteActionCommand(action, 200)).Execute();
            IoCContainer.Resolve<ICommand>(IoCStringConstants.GlobalCommandsQueueAdd, new ActionCommand(() => { barrier.SignalAndWait(); })).Execute();

            IoCContainer.Resolve<ICommand>("CreateNewEventLoop", "el").Execute();

            barrier.SignalAndWait();

            changeableValue.Should().Be(200);
            globalQueue.Should().HaveCount(0);
        }
        [Fact]
        public void Soft_stop_breaks_event_loop_complex_sub_command()
        {
            var globalQueue = new BlockingCollection<ICommand>();
            var barrier = new Barrier(2);

            ScopeManager.SetCurrentScope(ScopeManager.DefaultScope);

            IoCContainer.Resolve<ICommand>(IoCStringConstants.Register, IoCStringConstants.GlobalCommandsQueue, globalQueue).Execute();
            IoCContainer.Resolve<ICommand>(IoCStringConstants.Register, IoCStringConstants.GlobalCommandsQueueAdd, (object[] args) =>
            {
                IoCContainer.Resolve<BlockingCollection<ICommand>>(IoCStringConstants.GlobalCommandsQueue).Add((ICommand)args[0]);
            }).Execute();
            IoCContainer.Resolve<ICommand>(IoCStringConstants.Register, "CreateNewEventLoop", (object[] args) => {
                EventLoopManager.StartEventLoop((string)args[0]);
            }).Execute();

            int changeableValue = 10;
            var action = (int a) => { changeableValue = a; };
            var action2 = (int a) => { 
                changeableValue = a;
                IoCContainer.Resolve<ICommand>(IoCStringConstants.GlobalCommandsQueueAdd, new ExecuteActionCommand(action, 300)).Execute();
                IoCContainer.Resolve<ICommand>(IoCStringConstants.GlobalCommandsQueueAdd, new ActionCommand(() => { barrier.SignalAndWait(); })).Execute();
            };

            IoCContainer.Resolve<ICommand>(IoCStringConstants.GlobalCommandsQueueAdd, new ExecuteActionCommand(action, 100)).Execute();

            IoCContainer.Resolve<ICommand>(IoCStringConstants.GlobalCommandsQueueAdd, new SoftStopCommand()).Execute();

            IoCContainer.Resolve<ICommand>(IoCStringConstants.GlobalCommandsQueueAdd, new ExecuteActionCommand(action2, 200)).Execute();

            IoCContainer.Resolve<ICommand>("CreateNewEventLoop", "el").Execute();

            barrier.SignalAndWait();

            changeableValue.Should().Be(300);
            globalQueue.Should().HaveCount(0);
        }
    }
}
