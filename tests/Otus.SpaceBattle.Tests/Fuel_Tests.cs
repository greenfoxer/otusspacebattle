﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Otus.SpaceBattle.Tests
{
    public class Fuel_Tests
    {
        [Fact]
        public void Check_fuel_when_fuel_enough()
        {
            //Arrange
            var mock = new Mock<IFuelable>();
            mock.SetupProperty<int>(x => x.Сonsumption, 5);
            mock.SetupProperty<int>(x => x.Fuel, 10);
            var fuelable = mock.Object;
            var sut = new CheckFuelCommand(fuelable);

            //Act
            Action act = () => { sut.Execute(); };

            //Assert
            act.Should().NotThrow();
        }
        [Fact]
        public void Check_fuel_and_throw_when_fuel_not_enough()
        {
            //Arrange
            var mock = new Mock<IFuelable>();
            mock.SetupProperty<int>(x => x.Сonsumption, 5);
            mock.SetupProperty<int>(x => x.Fuel, 3);
            var fuelable = mock.Object;
            var sut = new CheckFuelCommand(fuelable);

            //Act
            Action act = () => { sut.Execute(); };

            //Assert
            act.Should().Throw<CommandException>().WithMessage("Not enough fuel!");
        }
        [Fact]
        public void Burn_fuel()
        {
            //Arrange
            Dictionary<string, object> data = new Dictionary<string, object>() { { "Сonsumption", 5 }, { "Fuel", 20 } };

            var uobject = new Mock<IUObject>();
            uobject.Setup(x => x.GetProperty("Fuel")).Returns(data["Fuel"]);
            uobject.Setup(x => x.GetProperty("Сonsumption")).Returns(data["Сonsumption"]);
            uobject.Setup(x => x.SetProperty(It.IsAny<string>(), It.IsAny<object>()))
                .Callback<string, object>((a, b) => data[a] = b)
                .Returns(true);
            var obj = uobject.Object;

            var fuelable = new FuelableAdapter(obj);
            var sut = new BurnFuelCommand(fuelable);

            //Act
            sut.Execute();

            //Assert
            data["Fuel"].Should().Be(15);
        }
    }
}
