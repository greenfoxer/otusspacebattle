﻿using Otus.SpaceBattle.IoC.Realisations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Otus.SpaceBattle.Tests
{
    public class IoC_ScopeManager_Tests
    {
        [Fact]
        public void Manager_creates_scopes()
        {
            var scope = ScopeManager.CreateNew("Game");

            scope.Should().NotBeNull();

            scope.Add("test", 2);

            scope["test"].Should().Be(2);
        }
        [Fact]
        public void Manager_creates_hierarchy_scopes()
        {
            ScopeManager.GetCurrentScope().Add("RootDependency", 5);

            var newScope = ScopeManager.CreateNew("ChildScope", ScopeManager.GetCurrentScope());

            var rd = newScope["RootDependency"];

            rd.Should().Be(5);
        }
        [Fact]
        public void Manager_creates_scopes_in_multitread()
        {
            ScopeManager.GetCurrentScope().Add("test", 1);
            var scope1 = new Scope("p1");
            scope1.Add("test", 10);
            var scope2 = new Scope("p2");
            scope2.Add("test", 1000);


            Barrier barrier = new Barrier(2);

            Thread thread1 = new Thread(() =>
            {
                ScopeManager.SetCurrentScope(scope1);
                barrier.SignalAndWait();
                ScopeManager.GetCurrentScope().Should().BeSameAs(scope1);
            });
            Thread thread2 = new Thread(() =>
            {
                ScopeManager.SetCurrentScope(scope2);
                barrier.SignalAndWait();
                ScopeManager.GetCurrentScope().Should().BeSameAs(scope2);
            });

            thread1.Start();
            thread2.Start();

            thread1.Join();
            thread2.Join();


            ScopeManager.GetCurrentScope()["test"].Should().Be(1);

        }
    }
}
