﻿using Otus.SpaceBattle.IoC.Exceptions;
using Otus.SpaceBattle.IoC.Realisations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static Otus.SpaceBattle.Tests.IoC_Tests;

namespace Otus.SpaceBattle.Tests
{
    public class IoC_Scope_Tests
    {
        [Fact]
        public void Access_to_scope_by_existing_value_dependency_name_returns_value()
        {
            var scope = new Scope("TestScope");
            int a = 2;
            scope.Add("int", a);

            var result = scope["int"];

            result.Should().Be(a);
        }
        [Fact]
        public void Adding_to_scope_doubled_dependency_throws()
        {
            var scope = new Scope("TestScope");
            int a = 2;
            scope.Add("int", a);

            scope.Add("int", 3);

            scope["int"].Should().Be(3);
        }
        [Fact]
        public void Access_to_scope_by_existing_dependency_name_returns_value()
        {
          
            var scope = new Scope("TestScope");
            var a = new A();
            scope.Add("A", a);

            var result = scope["A"];

            result.Should().Be(a);
            result.Should().BeSameAs(a);
        }
        [Fact]
        public void When_adding_empty_key_for_dependency_throws_scope_exception()
        {

            var scope = new Scope("TestScope");
            Action act = () => scope.Add(null, 3);

            act.Should().Throw<ScopeException>().WithMessage("Dependency key could not be null or empty!");
        }
    }
}
