﻿
using Castle.Components.DictionaryAdapter.Xml;
using Otus.SpaceBattle.IoC.Exceptions;
using Otus.SpaceBattle.IoC.IoCExecutionStrategies;
using Otus.SpaceBattle.IoC.Realisations;

namespace Otus.SpaceBattle.Tests
{
    public class IoC_Tests
    {

        [Fact]
        public void Register_dependency_as_singleton_value()
        {
            IoC.IoCContainer.Resolve<ICommand>(IoCStringConstants.Register, "2Int", 2).Execute();

            var result = IoCContainer.Resolve<int>("2Int");


            result.Should().Be(2);
        }
        [Fact]
        public void Register_dependency_as_singleton_class_key_return_same_object()
        {
            var value = new A();
            IoC.IoCContainer.Resolve<ICommand>(IoCStringConstants.Register, "A", value).Execute();

            var result1 = IoC.IoCContainer.Resolve<A>("A");
            var result2 = IoC.IoCContainer.Resolve<A>("A");

            result1.Should().BeSameAs(value);
            result2.Should().BeSameAs(result1); 
        }
        [Fact]
        public void Register_dependency_as_fabric_method_same_key_return_different_object()
        {
            IoC.IoCContainer.Resolve<ICommand>(IoCStringConstants.Register, "new A", () => { return new A(); }).Execute();

            var result1 = IoC.IoCContainer.Resolve<A>("new A");
            var result2 = IoC.IoCContainer.Resolve<A>("new A");

            result1.Number.Should().Be(1);
            result2.Number.Should().Be(2);
        }

        [Fact]
        public void Register_new_strategy_for_nonparametrized_action_and_register_action_should_work()
        {
            object obj = 42;
            try
            {
                IoC.IoCContainer.RegisterStrategy(new NonParametrizedActionToCommandStrategy());
            }
            catch
            { }
            IoC.IoCContainer.Resolve<ICommand>(IoCStringConstants.Register, "Action", () => { obj = 100; }).Execute();

            IoC.IoCContainer.Resolve<ICommand>("Action").Execute();

            obj.Should().Be(100);
        }
        [Fact]
        public void Register_new_strategy_for_parametrized_action_and_register_action_should_work()
        {
            object obj = 42;
            var a = new A();
            try
            {
                IoC.IoCContainer.RegisterStrategy(new ParametrizedActionToCommandStrategy());
            }
            catch {  }
            IoC.IoCContainer.Resolve<ICommand>(IoCStringConstants.Register, "ActionWithParams", (object[] param) => { obj = param[0]; }).Execute();

            IoC.IoCContainer.Resolve<ICommand>("ActionWithParams", a).Execute();

            obj.Should().BeSameAs(a);
        }

        [Fact]
        public void Multithread_work()
        {
            Barrier barrier = new Barrier(2);

            Thread thread1 = new Thread(() =>
            {
                ScopeManager.CreateNew("player1");
                var cmd = IoC.IoCContainer.Resolve<ICommand>(IoCStringConstants.Register, "2Int", 1);
                barrier.SignalAndWait();
                cmd.Execute();
                var result = IoC.IoCContainer.Resolve<int>("2Int");
                result.Should().Be(1);
            });
            Thread thread2 = new Thread(() =>
            {
                var cmd = IoC.IoCContainer.Resolve<ICommand>(IoCStringConstants.Register, "2Int", 2);
                barrier.SignalAndWait();
                cmd.Execute();
                var result = IoC.IoCContainer.Resolve<int>("2Int");
                result.Should().Be(2);
            });

            thread1.Start();
            thread2.Start();

            thread1.Join();
            thread2.Join();

        }
        [Fact]
        public void Wrong_dependency_key_throws()
        {
            int result;

            var act = () => { result = IoC.IoCContainer.Resolve<int>(String.Empty); } ;

            act.Should().Throw<IoCException>();
        }
        //[Fact]
        //public void Attempting_to_register_two_cast_strategies_of_one_type_throws()
        //{
        //    IoC.IoCContainer.RegisterStrategy(new NonParametrizedActionToCommandStrategy());

        //    var act = () => { IoC.IoCContainer.RegisterStrategy(new NonParametrizedActionToCommandStrategy()); };

        //    act.Should().Throw<IoCException>();
        //}
    }
}
