﻿using Moq;
using Otus.SpaceBattle.Core.Abstractions;
using Otus.SpaceBattle.Core.Commands;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace Otus.SpaceBattle.Tests
{
    public class Macrocommands_Tests
    {

        [Fact]
        public void Rotate_and_move_macrocommand_rotates_and_moves_object()
        {
            //Arrange
            Dictionary<string, object> data = new Dictionary<string, object>() {
                { "Position", new Vector2(12, 5) },
                { "Velocity", 8 },
                { "MaxDirections", 72 },
                { "Direction", 31 },
                { "AngularVelocity", 5 },
                { "Сonsumption", 5 },
                { "Fuel", 20 }
            };

            
            var obj = new UobjectMoq(data);

            var fuelable = new FuelableAdapter(obj);
            var movable = new MovableAdapter(obj);
            var rotable = new RotateAdapter(obj);

            var checkFuelCmd = new CheckFuelCommand(fuelable);
            var burnFuelCmd = new BurnFuelCommand(fuelable);
            var moveCmd = new MoveCommand(movable);
            var rotateCmd = new RotateCommand(rotable);

            var sut = new LinearMacrocommand(checkFuelCmd, burnFuelCmd, rotateCmd, moveCmd);

            //Act 
            sut.Execute();

            //Assert
            data["Position"].Should().Be(new Vector2(4, 5));
        }
        [Fact]
        public void Move_macrocommand_moves_object()
        {
            //Arrange

            Dictionary<string, object> data = new Dictionary<string, object>() {
                { "Position", new Vector2(12, 5) }, 
                { "Velocity", 8 }, 
                { "MaxDirections", 72 }, 
                { "Direction", 31 },
                { "AngularVelocity", 5 },
                { "Сonsumption", 5 }, 
                { "Fuel", 20 }
            };

            var uobject = new Mock<IUObject>();
            uobject.Setup(x => x.GetProperty("Position")).Returns(data["Position"]);
            uobject.Setup(x => x.GetProperty("Velocity")).Returns(data["Velocity"]);
            uobject.Setup(x => x.GetProperty("MaxDirections")).Returns(data["MaxDirections"]);
            uobject.Setup(x => x.GetProperty("Direction")).Returns(data["Direction"]);
            uobject.Setup(x => x.GetProperty("Fuel")).Returns(data["Fuel"]);
            uobject.Setup(x => x.GetProperty("Сonsumption")).Returns(data["Сonsumption"]);
            uobject.Setup(x => x.GetProperty("AngularVelocity")).Returns(data["AngularVelocity"]);

            uobject.Setup(x => x.SetProperty(It.IsAny<string>(), It.IsAny<object>()))
                .Callback<string, object>((a, b) => data[a] = b)
                .Returns(true);
            var obj = uobject.Object;

            var fuelable = new FuelableAdapter(obj);
            var movable = new MovableAdapter(obj);
            var rotable = new RotateAdapter(obj);

            var checkFuelCmd = new CheckFuelCommand(fuelable);
            var burnFuelCmd = new BurnFuelCommand(fuelable);
            var moveCmd = new MoveCommand(movable);

            var sut = new LinearMacrocommand(checkFuelCmd, burnFuelCmd, moveCmd);

            //Act 
            sut.Execute();

            //Assert
            data["Position"].Should().Be(new Vector2(5, 8));
        }
        [Fact]
        public void Move_macrocommand_fails_when_not_enough_fuel()
        {
            //Arrange

            Dictionary<string, object> data = new Dictionary<string, object>() {
                { "Position", new Vector2(12, 5) },
                { "Velocity", 8 },
                { "MaxDirections", 72 },
                { "Direction", 31 },
                { "AngularVelocity", 5 },
                { "Сonsumption", 5 },
                { "Fuel", 3 }
            };

            var uobject = new Mock<IUObject>();
            uobject.Setup(x => x.GetProperty("Position")).Returns(data["Position"]);
            uobject.Setup(x => x.GetProperty("Velocity")).Returns(data["Velocity"]);
            uobject.Setup(x => x.GetProperty("MaxDirections")).Returns(data["MaxDirections"]);
            uobject.Setup(x => x.GetProperty("Direction")).Returns(data["Direction"]);
            uobject.Setup(x => x.GetProperty("Fuel")).Returns(data["Fuel"]);
            uobject.Setup(x => x.GetProperty("Сonsumption")).Returns(data["Сonsumption"]);
            uobject.Setup(x => x.GetProperty("AngularVelocity")).Returns(data["AngularVelocity"]);

            uobject.Setup(x => x.SetProperty(It.IsAny<string>(), It.IsAny<object>()))
                .Callback<string, object>((a, b) => data[a] = b)
                .Returns(true);
            var obj = uobject.Object;

            var fuelable = new FuelableAdapter(obj);
            var movable = new MovableAdapter(obj);
            var rotable = new RotateAdapter(obj);

            var checkFuelCmd = new CheckFuelCommand(fuelable);
            var burnFuelCmd = new BurnFuelCommand(fuelable);
            var moveCmd = new MoveCommand(movable);

            var sut = new LinearMacrocommand(checkFuelCmd, burnFuelCmd, moveCmd);

            //Act
            Action act = () => { sut.Execute(); };

            //Assert
            act.Should().Throw<CommandException>().WithMessage("Not enough fuel!");
        }
    }
}
