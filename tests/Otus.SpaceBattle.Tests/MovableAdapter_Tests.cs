﻿namespace Otus.SpaceBattle.Tests
{
    public class MovableAdapter_Tests
    {
        [Fact]
        public void Moving_object_from_one_point_to_another()
        {
            //Arrange

            Dictionary<string, object> data = new Dictionary<string, object>() { { "Position", new Vector2(12, 5) }, { "Velocity", 8 }, { "MaxDirections", 72 }, { "Direction", 31 } };

            var uobject = new Mock<IUObject>();
            uobject.Setup(x => x.GetProperty("Position")).Returns(data["Position"]);
            uobject.Setup(x => x.GetProperty("Velocity")).Returns(data["Velocity"]);
            uobject.Setup(x => x.GetProperty("MaxDirections")).Returns(data["MaxDirections"]);
            uobject.Setup(x => x.GetProperty("Direction")).Returns(data["Direction"]);
            uobject.Setup(x => x.SetProperty(It.IsAny<string>(), It.IsAny<object>()))
                .Callback<string, object>((a, b) => data[a] = b)
                .Returns(true);
            var movable = uobject.Object;

            var sut = new MovableAdapter(movable);

            //Act
            sut.Position += sut.Velocity;

            //Assert

            data["Position"].Should().Be(new Vector2(5, 8));
        }
        [Fact]
        public void Trying_to_read_postition_from_movable_object_without_position()
        {
            //Arrange
            var uobject = new Mock<IUObject>();
            uobject.Setup(x => x.GetProperty("Position")).Returns(null);
            var sut = new MovableAdapter(uobject.Object);

            //Act
            Action act = () => { var x = sut.Position; };

            //Assert
            act.Should().Throw<MissingFieldException>().WithMessage("Position");
        }
        [Fact]
        public void Trying_to_read_velocity_from_movable_object_without_velocity()
        {
            //Arrange
            List<string> possibleFails = new List<string>() { "Velocity", "MaxDirections", "Direction" };
            var uobject = new Mock<IUObject>();
            uobject.Setup(x => x.GetProperty("Velocity")).Returns(null);
            var sut = new MovableAdapter(uobject.Object);

            //Act
            Action act = () => { var x = sut.Velocity; };

            //Assert
            act.Should().Throw<MissingFieldException>().Where(t => possibleFails.Contains(t.Message));
        }
        [Fact]
        public void Trying_to_set_position_for_object_with_immutable_position() // непонятно из задания, имеется ли в виду, что у объекта нет поля Position, или что его просто по какой-то причине нельзя изменить
        {
            //Arrange
            var uobject = new Mock<IUObject>();
            uobject.Setup(x => x.GetProperty("Position")).Returns(new Vector2(12, 8));
            uobject.Setup(x => x.SetProperty("Position", It.IsAny<object>())).Returns(false);
            var sut = new MovableAdapter(uobject.Object);

            //Act
            Action act = () => { sut.Position += new Vector2(3, 4); };

            //Assert
            act.Should().Throw<InvalidOperationException>().WithMessage("Position");
        }
        [Fact]
        public void Trying_to_set_position_for_object_without_position() // непонятно из задания, имеется ли в виду, что у объекта нет поля Position, или что его просто по какой-то причине нельзя изменить
        {
            //Arrange
            var uobject = new Mock<IUObject>();
            uobject.Setup(x => x.GetProperty("Position")).Returns(null);
            var sut = new MovableAdapter(uobject.Object);

            //Act
            Action act = () => { sut.Position += new Vector2(3, 4); };

            //Assert
            act.Should().Throw<MissingFieldException>().WithMessage("Position");
        }
        [Fact]
        public void When_movable_sets_velocity_vector_velocity_components_changes_in_data()
        {
            //Arrange
            Dictionary<string, object> data = new Dictionary<string, object>() {
                { "Velocity", 0 },
                { "MaxDirections", 72 },
                { "Direction", 0 }
            };


            var obj = new UobjectMoq(data); 

            var movable = new MovableAdapter(obj);
            var newVelocity = new Vector2(-7, 3);

            //Act 
            movable.Velocity = newVelocity;

            //Assert
            obj.GetProperty("Velocity").Should().Be(8);
            obj.GetProperty("Direction").Should().Be(31);
        }
    }
}
