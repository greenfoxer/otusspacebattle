﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Otus.SpaceBattle.Tests
{
    public class RotableAdapter_Tests
    {
        [Theory]
        [InlineData(1, 8, 3, 4)]
        [InlineData(5, 8, 3, 0)]
        [InlineData(10, 8, 3, 5)]
        [InlineData(-10, 8, 3, 1)]
        public void Rotate_object_in_segments_with_angular_velocity(int angularVelocity, int maxDirections, int currentDirection, int expectedDirection)
        {
            //Arrange
            Dictionary<string, object> data = new Dictionary<string, object>() { { "AngularVelocity", angularVelocity }, { "MaxDirections", maxDirections }, { "Direction", currentDirection } };

            var uobject = new Mock<IUObject>();
            uobject.Setup(x => x.GetProperty("AngularVelocity")).Returns(data["AngularVelocity"]);
            uobject.Setup(x => x.GetProperty("MaxDirections")).Returns(data["MaxDirections"]);
            uobject.Setup(x => x.GetProperty("Direction")).Returns(data["Direction"]);
            uobject.Setup(x => x.SetProperty(It.IsAny<string>(), It.IsAny<object>()))
                .Callback<string, object>((a, b) => data[a] = b)
                .Returns(true);

            IRotable sut = new RotateAdapter(uobject.Object);

            //Act
            sut.Direction += sut.AngularVelocity ;

            //Assert
            data["Direction"].Should().Be(expectedDirection);
        }

        [Fact]
        public void Trying_to_read_angular_velocity_from_rotable_object_without_angular_velocity()
        {
            //Arrange
            var uobject = new Mock<IUObject>();
            uobject.Setup(x => x.GetProperty("AngularVelocity")).Returns(null);
            var sut = new RotateAdapter(uobject.Object);

            //Act
            Action act = () => { var x = sut.AngularVelocity; };

            //Assert
            act.Should().Throw<MissingFieldException>().WithMessage("AngularVelocity");
        }
        [Fact]
        public void Trying_to_set_position_for_object_with_immutable_current_direction() // непонятно из задания, имеется ли в виду, что у объекта нет поля Position, или что его просто по какой-то причине нельзя изменить
        {
            //Arrange
            var uobject = new Mock<IUObject>();
            uobject.Setup(x => x.GetProperty("Direction")).Returns(1);
            uobject.Setup(x => x.GetProperty("MaxDirections")).Returns(8);
            uobject.Setup(x => x.SetProperty("Direction", It.IsAny<object>())).Returns(false);
            var sut = new RotateAdapter(uobject.Object);

            //Act
            Action act = () => { sut.Direction += 1; };

            //Assert
            act.Should().Throw<InvalidOperationException>().WithMessage("Direction");
        }
    }
}
