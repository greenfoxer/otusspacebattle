﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Otus.SpaceBattle.Tests
{
    internal class UobjectMoq : IUObject
    {
        private Dictionary<string, object> _object;
        public UobjectMoq(Dictionary<string, object> description)
        {
            _object = description;
        }
        public object GetProperty(string prop)
        {
            if (_object.TryGetValue(prop, out var value))
                return value;
            return null;
        }

        public bool SetProperty(string prop, object value)
        {
            if (_object.ContainsKey(prop)) { 
            _object[prop] = value;
                return true;
            }
            return false;
        }
    }
}
